# S-Cape

## Description
S-Cape is a game in virtual reality made with Unity. You will need to find clues and resolve riddles to escape those labyrinthes and find back you memories.

Game Design Document (french): https://www.canva.com/design/DAEzLIMi5uU/1yMkfXKof-UGxSCHzMZoBw/view?utm_content=DAEzLIMi5uU&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton

Trailer : https://www.youtube.com/watch?v=JQkvlCB22XI&ab_channel=Krakene

## Features
- 4 Levels finished

## Requirements
Unity version [to define] or higher

## Getting Started
- Clone or download the repository
- Open the project in Unity
- Play the scene in the Unity Editor

## Controls
- Hold up and Orient remote / Release : Position teleport's cursor / Teleport
-  Hold / Release side and back button : Grab object / Release Object 
- Press left and right : Orient view

For better understanding, please check the schema in *Controls* part of the game design document.



## Assets
Low Poly Dungeon by JustCreate : https://assetstore.unity.com/packages/3d/environments/dungeons/low-poly-dungeons-176350 

Assets used in this game project are either created by the developers or obtained through authorized sources mentionned above. All Assets are used in accordance with the copyright laws and with the proper permissions from the respective copyright owners.

The developers claim no ownership over any third-party images used in this project and any associated copyrights remain with the respective copyright owners.

If you believe that any images used in this project infringe upon your copyright, please contact us immediately at julie.celli31@gmail.com. We will take appropriate action to rectify the situation.