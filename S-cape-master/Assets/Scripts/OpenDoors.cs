﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoors : MonoBehaviour
{
    [SerializeField] private GameObject leftDoor, rightDoor;
    [SerializeField] private AudioClip sound;
    private Animator amLeft, amRight;

    void Start()
    {
        amLeft = leftDoor.GetComponent<Animator>();
        amRight = rightDoor.GetComponent<Animator>();
    }

    public void AnimateOpenDoors()
    {
        AudioSource.PlayClipAtPoint(sound, transform.position);
        amLeft.SetTrigger("openDoor");
        amRight.SetTrigger("openDoor");
    }
}
