﻿using System.Collections;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    public string tags;
    [SerializeField] private GameObject m_gameObject;
    [SerializeField] private GameObject m_wall;
    [SerializeField] private Canvas canvas;
    [SerializeField] private AudioClip sound;
    [SerializeField] private Animator am;
    public GameObject doorRestriction;

    private bool doorOpen = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(tags) && !doorOpen)
        {
            AudioSource.PlayClipAtPoint(sound, transform.position);
            am.SetTrigger("openDoor");
            if (m_wall)
            {
                m_wall.GetComponent<MeshRenderer>().enabled = false;
                m_wall.GetComponent<MeshCollider>().enabled = false;
            }
            if (m_gameObject)
            {
                m_gameObject.GetComponent<BoxCollider>().enabled = false;
            }
            if (canvas)
            {
                canvas.gameObject.SetActive(false);
            }
            if(doorRestriction){
                doorRestriction.SetActive(false);
            }
            doorOpen = true;
        }
    }

}

