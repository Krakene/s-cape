﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonTransitioner : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler,IPointerDownHandler, IPointerClickHandler 
{
    public Color32 NormalColor = Color.white; 
    public Color32 HoverColor = new Color32(152, 152, 152, 60); //#989898
    public Color32 DownColor = new Color32(87, 78, 78, 20); //#818380
    private Image _image = null;

    private void Awake()
    {
        _image = GetComponent<Image>();
    }

    public void OnPointerEnter(PointerEventData eventData) 
    {
        //laisser les prints ?
        print("Enter");
        _image.color = HoverColor;
    }

    public void OnPointerExit(PointerEventData eventData) 
    {
        print("Exit");
         _image.color = NormalColor;
    }

    public void OnPointerDown(PointerEventData eventData) 
    {
        print("Down");
        _image.color = DownColor;
    }

    public void OnPointerUp(PointerEventData eventData) 
    {
        print("Up");
    }

    public void OnPointerClick(PointerEventData eventData) 
    {
        print("Click");
        _image.color = HoverColor;
    }
}
