using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageInventorySlots : MonoBehaviour
{
    public int coinsCount;
    private string slotUsed = "";

    public static ManageInventorySlots instance;

    //instanciation du singleton pour gérer le slot en cours d'utilisation
    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Il y a plus d'une instance de Inventory dans la scène");
        }

        instance = this;
    }

    public void SetSlothUsed(string objectName)
    {
        if (slotUsed == objectName)
        {
            return;
        }
        slotUsed = objectName;
    }

    public void ResetSlotUsed()
    {
        slotUsed = "";
    }

    public string GetSlotUsed()
    {
        return slotUsed;
    }
}
