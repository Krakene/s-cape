﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnLightOn : MonoBehaviour
{
    [SerializeField] private string tags;
    [SerializeField] private GameObject m_candleLight;
    [SerializeField] private GameObject m_particuleFire;
    [SerializeField] private AudioClip soundLightUp;
    [SerializeField] private AudioSource soundLit;

    private bool lightOn = false;



    private void OnTriggerEnter(Collider other)
    {
            
        if (other.CompareTag(tags) && !lightOn)
        {
            AudioSource.PlayClipAtPoint(soundLightUp, transform.position);
            soundLit.Play();
            m_candleLight.GetComponent<Light>().enabled = true;
            m_particuleFire.SetActive(true);
            lightOn = true;
        }
            

    }



    
}
